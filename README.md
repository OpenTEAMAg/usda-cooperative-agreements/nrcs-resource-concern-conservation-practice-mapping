# NRCS Resource Concern - Conservation Practice Mapping

## Ownership:
- NRCS, Contact: Lynn Knight (lynn.knight@usda.gov)

## Authorship: 
> when, who, what, acknowledgement
- 2023, Eric McTaggert, CART, Originating Data
- 2023, Juliet Norton, USDA NRCS Cooperative Agreement - Award #: NR223A750008C003
   - Data Relationships in Airtable
   - Airtable-Gitlab Distribution Pipeline 

## Description:
- This is the FY 2023 data for NRCS Conservation Practices and which Resource Concerns they address.
- Data formats: 
   - csv
   - json (coming soon)
- In the Airtable section below you can find a link to a template base this data can be imported into. Data can be imported into the template airtable base in the following ways:
   - import the CSV files through the native Airtable interface
   - import the json files through the API. (Coming Soon)

## Terms of Use:
- You must receive permission from NRCS Contact to distribute
- You must preserve authorship list when distributing data resources

## Airtable Template:
Empty Template: https://www.airtable.com/universe/expnyEL5JyR7qXJB6/conservation-practice-resource-concerns-template 
