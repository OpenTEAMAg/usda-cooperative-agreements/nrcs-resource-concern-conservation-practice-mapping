# How to Contribute

## Data Content or Structure
To contribut changes to the CSV or JSON data files:
- Make changes to files in forked repository
- Add your name to the authorship list in the README.md
- Submit a merge request

## Airtable Template
If you have modifications to the airtable template, you will need to work with Juliet.

## Feedback and Feature Requests
If you had feedback for features, and don't want to contribute those changes, create an [issue](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/conservation-practice-resource-concerns/-/issues).
